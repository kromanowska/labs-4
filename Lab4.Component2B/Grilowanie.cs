﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Grilowanie : AbstractComponent, IGrzejnik
    {
        public Grilowanie()
        {
            //rejestruje interfejs dostarczany i jego instancje (this)
            RegisterProvidedInterface(typeof(IGrzejnik), this);
        }

        public void WlaczGrzanie()
        {
            Console.WriteLine("Grilowanie rozpoczete!");
        }

        public void WylaczGrzanie()
        {
            Console.WriteLine("Grilowanie zakonczone!");
        }

        //Komponent nie wymaga zadnego interfejsu, wiec implementacja tej metody jest niepotrzebna
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
