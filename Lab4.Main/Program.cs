﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2;
using Lab4.Contract;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            Container Kontener = new Container();
            Kontener.RegisterComponent(new Mikrofalowka());
            Kontener.RegisterComponent(new Rozmrazanie());
            IMikrofalowka mikorfala = Kontener.GetInterface<IMikrofalowka>();
            mikorfala.OtworzDrzwiczki();
            mikorfala.ZamknijDrzwiczki();
            mikorfala.Start();
            mikorfala.Stop();
            Console.ReadKey();
        }
    }
}
