﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Mikrofalowka : AbstractComponent, IMikrofalowka
    {
        IGrzejnik grzejnik;
        
        public Mikrofalowka()
        {
            //rejestruje interfejs dostarczany i jego instancje (this)
            RegisterProvidedInterface(typeof(IMikrofalowka), this);
            //rejestruje interfejs wymagany
            RegisterRequiredInterface(typeof(IGrzejnik));
        }

        //metoda wstrzykujaca interfejs wymagany przez ta klase
        public override void InjectInterface(Type type, object impl)
        {
            if (type ==  typeof(IGrzejnik))
            {
                this.grzejnik = (IGrzejnik)impl;
            }
        }

        //do tej metody korzystam z grzejnika
        public void Start()
        {
            grzejnik.WlaczGrzanie();
        }

        //do tej metody korzystam z grzejnika
        public void Stop()
        {
            grzejnik.WylaczGrzanie();
        }

        public void OtworzDrzwiczki()
        {
            Console.WriteLine("Drzwi otwarte!");   
        }

        public void ZamknijDrzwiczki()
        {
            Console.WriteLine("Drzwi zamkniete!");
        }
    }
}
